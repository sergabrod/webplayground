from django.shortcuts import get_object_or_404
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from registration.models import Profile

# Create your views here.
class ProfileListView(ListView):
    # Definimos que model utilizará la clase
    # Para listar debemos crear el template con el nombre genérico 
    # profile_list.html y recorrer el objeto profile_list que será inyectado en él
    model = Profile
    template_name = 'profiles/profile_list.html'
    paginate_by = 3


class ProfileDetailView(DetailView):
    model = Profile
    template_name = 'profiles/profile_detail.html'

    def get_object(self):
        return get_object_or_404(Profile, user__username=self.kwargs['username'])