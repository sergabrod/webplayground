from django.contrib import admin
from .models import Page

# Register your models here.
class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'order')

    # Inyectamos nuestro css customizado para que el ckeditor se adpate a pantallas mobile u otra forma mas sencilla es:
    # Hola, solo como alternativa pego aquí una forma para configurar que ckeditor se ajuste al ancho de la pantalla sin tener que usar un fichero css.

    # Para ello en settings.py hay que añadir lo siguiente:

    # CKEDITOR_CONFIGS = {
    #     'default': {
    #         'width'  : 'auto',
    #         'height' : 'auto'
    #     }
    # }
    class Media:
        css = {
            'all': ('pages/css/custom_ckeditor.css',)
        }

admin.site.register(Page, PageAdmin)
