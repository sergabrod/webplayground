from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy
from django.shortcuts import redirect
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
from .models import Page
from .forms import PageForm

"""
    También existen otras funciones decoradoras útilies aparte de staff_member_required, son: 
    login_required (cualquier usuario no solo staff) y permmission_required (logueado y con un cierto permiso).
    Además también podemor decorar directamente en el url pattern. Ver recursos adjuntos
"""

class StaffRequiredMixin(object):
    #Este mixin chequea que el usuario sea miembro del staff

    #Si el usuario no está logueado o no es admin, redireccionamos al login con el decorator staff_member_required
    @method_decorator(staff_member_required)    
    def dispatch(self, request, *args, **kwargs):
        return super(StaffRequiredMixin, self).dispatch(request, *args, **kwargs)


# Create your views here.
class PageListView(ListView):
    # Definimos que model utilizará la clase
    # Para listar debemos crear el template con el nombre genérico 
    # page_list.html y recorrer el objeto page_list que será inyectado en él
    model = Page


class PageDetailView(DetailView):
    model = Page


# Sin usar mixin, decoramos la clase con staf_member_required y le decirmos que método vamos a decorar, en este caso "dispatch"
@method_decorator(staff_member_required, name='dispatch')
class PageCreate(CreateView):
    model = Page
    form_class = PageForm
    #fields = ['title', 'content', 'order'] Eliminado al comenzar a utilizar los forms basados en CBV Forms
    # Sobreescribimos este método para redireccionar al listado de pages una vez creada la nueva page
    # en vez de sobreescribir mejor utilizamos el reverse_lazy
    # def get_success_url(self):
    #     return reverse('pages:pages'
    success_url = reverse_lazy('pages:pages')


@method_decorator(staff_member_required, name='dispatch')
class PageUpdate(UpdateView):
    model = Page
    form_class = PageForm
    template_name_suffix = '_update_form'
    #success_url = reverse_lazy('pages:pages')

    #Como al terminar de hacer el update queremos que nos muestre nuevamente el formulario para chequear
    #que los cambios se realizaron correctamente, debemos pasar el paràmetro id y para ello
    #no podemos usar directamente el reverse_lazy como lo hiciemos en el CreateView, sino que debemos sobreescribir
    #el método get_success_url mediante el cual podemos acceder a self
    def get_success_url(self):
        return reverse_lazy('pages:update', args=[self.object.id]) + '?ok'

@method_decorator(staff_member_required, name='dispatch')    
class PageDelete(DeleteView):
    model = Page
    success_url = reverse_lazy('pages:pages')

