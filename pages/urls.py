from django.urls import path
from .views import PageListView, PageDetailView, PageCreate, PageUpdate, PageDelete

# pages_patterns es un nombre cualquiera elegido, no es una lista sino una tupla
pages_patterns = ([
    path('', PageListView.as_view(), name='pages'),
    # pk indica que el parámentro enviado es primary key del model
    path('<int:pk>/<slug:page_slug>/', PageDetailView.as_view(), name='detail'),
    path('create/', PageCreate.as_view(), name='create'),
    path('update/<int:pk>/', PageUpdate.as_view(), name='update'),
    path('delete/<int:pk>/', PageDelete.as_view(), name='delete'),
], 'pages')
