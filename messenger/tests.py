from django.test import TestCase
from django.contrib.auth.models import User
from .models import Thread, Message

# Create your tests here.
# Para correr el test podriamos hacer:
# python manage.py test messenger: Corre todos los test de la app messenger
# python manage.py test messenger.tests.ThreadTestCase: corre los test de la clase indicada
# python manage.py test messenger.tests.ThreadTestCase.test_add_users_to_thread: corre solo ese test

class ThreadTestCase(TestCase):
    def setUp(self):
        # inicializamos el test creando dos usuarios y un hilo
        self.user1 = User.objects.create_user('user1', None, 'test1234')
        self.user2 = User.objects.create_user('user2', None, 'test1234')
        self.user3 = User.objects.create_user('user3', None, 'test1234')

        self.thread = Thread.objects.create()

   
    def test_add_users_to_thread(self):
        # Añadimos dos usuarios al hilo, usando la relacion many 2 many
        self.thread.users.add(self.user1, self.user2)
        #comprobamos el resultado con assert, primero verificando que 
        # existen dos usuarios en el thread
        self.assertEqual(len(self.thread.users.all()), 2)

    # Test que recupera un hilo a partir de sus usuarios
    def test_filter_thread_by_users(self):
        self.thread.users.add(self.user1, self.user2)
        threads = Thread.objects.filter(users=self.user1).filter(users=self.user2)
        self.assertEqual(self.thread, threads[0])

    # Al revés que el anterior ahroa testeamos que no existe un hilo para determinados usuarios
    def test_filter_non_existent_thread(self):
        threads = Thread.objects.filter(users=self.user1).filter(users=self.user2)
        self.assertEqual(len(threads), 0)

    # testeamos si los mensajes son añadidos correctamente al hilo
    def test_add_messages_to_thread(self):
        self.thread.users.add(self.user1, self.user2)
        message1 = Message.objects.create(user=self.user1, content='hola cómo estás')
        message2 = Message.objects.create(user=self.user2, content='muy bien, trabajando')
        self.thread.messages.add(message1, message2)
        self.assertEqual(len(self.thread.messages.all()), 2)

        for message in self.thread.messages.all():
            print("({}): {}".format(message.user, message.content))

    def test_add_message_from_user_not_in_thread(self):
        self.thread.users.add(self.user1, self.user2)
        message1 = Message.objects.create(user=self.user1, content='hola cómo estás')
        message2 = Message.objects.create(user=self.user2, content='muy bien, trabajando, y vos?')
        message4 = Message.objects.create(user=self.user1, content='yo de vacaciones')
        message3 = Message.objects.create(user=self.user3, content='Soy un espía que no pertenezco al thread')
        self.thread.messages.add(message1, message2, message3, message4)
        # debería haber sólo 2 messages ya que el message3 no pertenece a un usuario del thread
        # Esto fallará, para solucionarlo usaremos signals, más precisamente m2m_changed (many to many changed)
        # que la importaremos en el model desde django.db.models.signals
        self.assertEqual(len(self.thread.messages.all()), 3)

    
    def test_find_thread_with_custom_manager(self):
        self.thread.users.add(self.user1, self.user2)
        thread_model_manager = Thread.objects.find(self.user1, self.user2)
        self.assertEqual(self.thread, thread_model_manager)

    def test_find_or_create_thread_with_custom_manager(self):
        self.thread.users.add(self.user1, self.user2)
        #como el hilo ya existe, solo lo encontrará y será el creado al inicio
        thread_model_manager = Thread.objects.find_or_create(self.user1, self.user2)
        self.assertEqual(self.thread, thread_model_manager)

        # como no existe un hilo entre user1 y user3, deberá crearlo y no ser None
        thread_created = Thread.objects.find_or_create(self.user1, self.user3)
        self.assertIsNotNone(thread_created)


