from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from .models import Thread, Message
from django.http import Http404, JsonResponse

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.models import User
from django.urls import reverse_lazy

# Create your views here.
@method_decorator(login_required, name="dispatch")
class ThreadList(TemplateView):
    template_name = "messenger/thread_list.html"

    # Redefinimos el queryset de la clase, para poder obtener sólo aquellos hilos
    # del usuario logueado, pero solo a modo de ejemplo, hya que en nustro caso
    # como tenemos una relación, vamos a obtener los mensajes con user.threads.all()
    # def get_queryset(self):
    #     queryset = super(ThreadList, self).get_queryset()
    #     return queryset.filter(users=self.request.user)

@method_decorator(login_required, name="dispatch")
class ThreadDetail(DetailView):
    model = Thread

    # sobreescribimos el metodo get_object que es el metodo correlativo a Detail
    # ya que somo maneja una instancia, de esta manera el usuario activo sólo
    # puede ver su detalle
    def get_object(self):
        obj = super(ThreadDetail, self).get_object()
        if self.request.user not in obj.users.all():
            raise Http404()
        return obj

def add_message(request, pk):
    json_response = {'created':False}
    if request.user.is_authenticated:
        content = request.GET.get('content', None)
        if content:
            thread = get_object_or_404(Thread, pk=pk)
            message = Message.objects.create(user=request.user, content=content)
            thread.messages.add(message)
            json_response['created'] = True
            if len(thread.messages.all()) is 1:
                json_response['first'] = True
    else:
        raise Http404("User is not authenticad")
    return JsonResponse(json_response)

#como es una común no hace falta agregar tipo decorador
@login_required 
def start_thread(request, username):
    user = get_object_or_404(User, username=username)
    thread = Thread.objects.find_or_create(user, request.user)
    return redirect(reverse_lazy('messenger:detail', args=[thread.pk]))
    