from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import m2m_changed

# Create your models here.
class Message(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created']

class ThreadManager(models.Manager):
    
    def find(self, user1, user2):
        # dentro del manager, la palabra self siempre hace referencia  al queryset que contiene todas las instancias de ese modelo, 
        # en este caso sería equivalente a Thread.objects.all
        queryset = self.filter(users=user1).filter(users=user2)
        if len(queryset) > 0: # devolvió al menos un resultado
            return queryset[0]
        return None
    
    def find_or_create(self, user1, user2):
        thread = self.find(user1, user2)
        if thread is None: 
            thread = Thread.objects.create()
            thread.users.add(user1, user2)
        return thread
        
class Thread(models.Model):
    users = models.ManyToManyField(User, related_name='threads')
    messages = models.ManyToManyField(Message)
    updated = models.DateTimeField(auto_now=True)

    # con el ThreadManager podemos crear nuestros propios filtros en el model
    objects = ThreadManager()

    class Meta:
        ordering = ['-updated']

# definimos la función que usará la señal, si el mensaje no pertenece a un usario del Thread
# eliminamos el mensaje en el pre_post
def message_changed(sender, **kwargs):
    thread_instance = kwargs.pop("instance", None)
    #pre_add, post_add, etc.
    action = kwargs.pop("action", None)
    # almacenamos todas las pk de los mensajes que van a agregarse en la relación M2M, 
    # es una lista donde no se pueden duplicar elementos
    pk_set =  kwargs.pop("pk_set", None)
    print(thread_instance, action, pk_set)

    # si la accion es pre_add, es decir antes de guardar los mensajes en el thread, recorremos las claves
    # y obtenemos los mensajes y preguntamos si el usuario que lo envió, pertenece al thread
    # si no pertence, almacenamos la pk del mensaje en un set para luego removerlos
    false_pk_set = set()
    if action is 'pre_add':
        for msg_pk in pk_set:
            msg = Message.objects.get(pk=msg_pk)
            if msg.user not in thread_instance.users.all():
                print("Ups!, ({}) no forma parte del hilo ".format(msg.user))
                false_pk_set.add(msg_pk)
    
    #Removemos de pk_set los mensajes que están en false_pk_est, es decir los de usuarios que no pertenecen al hilo
    pk_set.difference_update(false_pk_set)

    # Forzar la actualización haciendo save para que actualice el updated
    thread_instance.save()


#https://docs.djangoproject.com/en/3.0/ref/signals/#m2m-changed
# conectaremos la señal con el Thread, ante cualquier cambio que suceda en él
m2m_changed.connect(message_changed, sender=Thread.messages.through)


# Creo un hilo con usuario y mensajes mediante la shell: python manage.py shell
""" from django.contrib.auth.models import User
from messenger.models import Thread, Message
gallardo = User.objects.get(username="gallardo")
ramon = User.objects.get(username="Ramon")
hilo = Thread.objects.find_or_create(gallardo, ramon)
hilo.messages.add(Message.objects.create(user=gallardo, content="Buenos días Ramón!, cómo estás?"))
hilo.messages.add(Message.objects.create(user=ramon, content="Muy bien Gallardo! y vos ?"))
hilo.messages.add(Message.objects.create(user=gallardo, content="Todo bien, necesito un arquero, vos podés jugar?"))
hilo.messages.add(Message.objects.create(user=ramon, content="Si! contá conmigo")) """

