from django.shortcuts import render
from django.views.generic.base import TemplateView

def home(request):
    return render(request, "core/home.html")

class HomePageView(TemplateView):

    template_name = "core/home.html"

    # Devuelve el render del template, le pasamos el diccionario de contexto
    # *args: argumentos, **kwargs: argumentos en clave valor
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'title':'Webplayground by Django'})
    


class SamplePageView(TemplateView):
    template_name = "core/sample.html"