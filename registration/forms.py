from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Profile

# Creamos un formulario de registro personalizado para agregar el campo email
# heredando de UserCreationForm, el campo email ya existe en el model User
# sólo le estamos diciendo a Django que lo utilice
class UserCreationFormWithEmail(UserCreationForm):
    email = forms.EmailField(required=True, help_text="Requerido, 254 caracteres como máximo y debe ser un email válido")

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")
    
    # El método debe comenzar con la palabra clean_fieldname
    def clean_email(self):
        # recuperamos el valor actual del campo email
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError("Este email ya se encuentra utilizado, ingrese uno diferente.")
        return email


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['avatar', 'bio', 'link']
        widgets = {
            'avatar': forms.ClearableFileInput(attrs={'class':'form-control-file mt-3'}),
            'bio': forms.Textarea(attrs={'class':'form-control mt-3', 'rows':3, 'placeholder':'Biografía'}),
            'link': forms.URLInput(attrs={'class':'form-control mt-3', 'placeholder':'Link'})
        }

class EmailForm(forms.ModelForm):
    email = forms.EmailField(required=True, help_text="Requerido, 254 caracteres como máximo y debe ser un email válido")

    class Meta:
        model = User
        fields = ['email']
    
    # El método debe comenzar con la palabra clean_fieldname
    def clean_email(self):
        # recuperamos el valor actual del campo email
        email = self.cleaned_data.get('email')
        # self.changed_data devuelve los valores que cambiaron
        if 'email' in self.changed_data:
            # Si el email cambió, entonces controlamos que ya ni exista
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError("Este email ya se encuentra utilizado, ingrese uno diferente.")
        return email