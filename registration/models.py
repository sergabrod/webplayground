from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

# Para poder eliminar la imágen avatar antigua, una vez que la actualizamos
# vamos a definir una función de upload de imágenes propias
# instance es la instancia con la nueva imágen, filename es la que vamos a sobreescribir
def custom_upload_to(instance, filename):
    old_instance = Profile.objects.get(pk=instance.pk)
    old_instance.avatar.delete()
    # una vez eliminada la imagen devolvemos el path con la nueva
    return 'profile/' + filename

# Profile relacionado al User, es una relación uno a uno
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to=custom_upload_to, null=True, blank=True)
    bio = models.TextField(null=True, blank=True)
    link = models.URLField(max_length=200, null=True, blank=True)

    class Meta:
        ordering = ['user__username']


# Signals: cada vez que se crea un usuario detectamos la señal 'post_save' del model User
# y creamos un perfil para ese usuario, de manera tal de que no haya usuarios
# sin perfil
@receiver(post_save, sender=User)
def ensure_profile_exists(sender, instance, **kwargs):
    # la señal post_save se ejecuta también en el update del model User, para crear el perfil
    # sòlo al crearse el usuario verificamos que kwargs tenga el parámetro 'created'
    if kwargs.get('created', False):
        Profile.objects.get_or_create(user=instance)
