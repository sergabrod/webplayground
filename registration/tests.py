from django.test import TestCase
from .models import Profile
from django.contrib.auth.models import User

# Create your tests here.
class ProfileTestCase(TestCase):

    # En el método setUp preparamos nuestra prueba
    def setUp(self):
        # Creamos un usuario para el test
        User.objects.create_user('test', 'test@gmail.com', 'test123123')

    # método donde definimos nuestra prueba misma
    # debe comenzar con test_
    def test_profile_exist(self):
        # buscamos si existe un profile para el usuario creado 
        exists = Profile.objects.filter(user__username='test').exists()
        self.assertEqual(exists, True)